const printMonthOfGivenDate=function(test_case)
{   if(test_case==null) return "Wrong Date";
    let monthNum=parseInt(test_case.toString().split("/")[1])
    let ans;
    switch(monthNum)
    {
        case 1:
            ans= "January";
            break;
        case 2:
            ans= "February";
            break;
        case 3:
            ans= "March";
            break;
        case 4:
            ans= "April";
            break;
        case 5:
            ans= "May";
            break;
        case 6:
            ans= "June";
            break;
        case 7:
            ans= "July";
            break;
        case 8:
            ans= "August";
            break;
        case 9:
            ans= "September";
            break;
        case 10:
            ans= "October";
            break;
        case 11:
            ans= "November";
            break;
        case 12:
            ans= "December";
            break;
        default:
            ans="Wrong date";
    }
    return ans;
}

module.exports={
    printMonthOfGivenDate
}
