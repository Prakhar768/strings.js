
function titleCase(str)
{
    
    str = str.toLowerCase().split(' ');
    for (var i = 0; i < str.length; i++)
    {
      str[i] = str[i].charAt(0).toUpperCase() + str[i].slice(1); 
    }
    return str.join(' ');
}
const fullNameTitleCase =function(test_string)
{
    let ans='';
    if(test_string.hasOwnProperty("firstName") ) ans += test_string.firstName.toUpperCase()
    if(test_string.hasOwnProperty("lastName") ) ans += test_string.lastName.toUpperCase()
    for (let value of Object.values(test_string)) {
        ans += value.toString();
        ans += " ";
    }
    ans=titleCase(ans);
    return ans;
}

module.exports={
    fullNameTitleCase
}
